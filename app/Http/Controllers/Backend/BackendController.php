<?php

namespace CleanBlog\Http\Controllers\Backend;

use Illuminate\Http\Request;
use CleanBlog\Http\Controllers\Controller;

class BackendController extends Controller
{
    //
    protected $data;
    protected $heading;
    protected $subheading;
    protected $vars = [];
}
