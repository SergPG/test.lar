<?php

namespace CleanBlog\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use CleanBlog\Http\Controllers\Backend\BackendController;
use CleanBlog\Http\Requests\StorePage;

use Config;

use CleanBlog\Models\Page;

class PageController extends BackendController
{

   public function __construct(Page $data){

     $this->data = $data;
   }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $pages = $this->data->all();
        $count = $this->data->count();

        //dd($count);

        $this->heading = 'Pages Panel';
        $this->subheading = 'Manage and Configure the Pages';

         $this->vars = array_add($this->vars,'heading',$this->heading);  
         $this->vars = array_add($this->vars,'subheading',$this->subheading);   
        $show_new_page = true;
         $this->vars = array_add($this->vars,'show_new_page',$show_new_page);

         $this->vars = array_add($this->vars,'pages',$pages);   


        return view('backend.pages.index')->with($this->vars );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
           session(['status' => 'Create']);
        // 
          $this->heading = 'Create Page';
        // $this->subheading = 'Manage and Configure the Pages';

         $this->vars = array_add($this->vars,'heading',$this->heading);  
        // $this->vars = array_add($this->vars,'subheading',$this->subheading);   


        // Для реализации Подменю 
        //  $count = $this->data->count();
        //  if ($count > 0) {
        //     $pages = $this->data->all();
        //     $this->vars = array_add($this->vars,'pages',$pages); 
        //  }
          
        return view('backend.pages.create')->with($this->vars );


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePage $request)
    {
        
        $page = new Page;

        $page->name = $request->name;

        $page->heading = $request->heading;

         if ($request->has('subheading')) {
            $page->subheading = $request->subheading;
          }
         
         if ($request->has('text')) {

         //   dd($request->text);
            $page->text = $request->text;
          }
      
          if ($request->hasFile('image')) {

                $path = $request->file('image')->store('pages','frontend');
              //  $url = config('site.storage_frontend').''.$path;
                $page->image = $path;
          }
   
          
          $result =   $page->save();           

 //$img = '<img class="img-fluid" src="'.asset($page->image).'" alt="">';

      //  return back();
     
       // dd($request);
          return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
          session(['status' => 'Edit']);
       //
         $this->heading = 'Page Edit';
        // $this->subheading = 'Manage and Configure the Pages';

         $this->vars = array_add($this->vars,'heading',$this->heading);  
        // $this->vars = array_add($this->vars,'subheading',$this->subheading);   


         $page = $this->data->find($id);
         $this->vars = array_add($this->vars,'page',$page);   

          
        return view('backend.pages.edit')->with($this->vars );
      

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorePage $request, $id)
    {
        //
         
         $page = $this->data->find($id);

         $old_imege = $page->image;

         $page->heading = $request->heading;

         if ($request->has('subheading')) {
            $page->subheading = $request->subheading;
          }
         
         if ($request->has('text')) {

         //   dd($request->text);
            $page->text = $request->text;
          }
      
          if ($request->hasFile('image')) {

            $exists = Storage::disk('frontend')->exists($old_imege);
           // dd($exists, $old_imege);
                
             if($exists) {
               $dfl =   Storage::disk('frontend')->delete($old_imege);

                //dd($dfl);

               } 
               
               
                $path = $request->file('image')->store('pages','frontend');
             //   $url = config('site.storage_frontend').''.$path;
                $page->image = $path;
          }
          else{
               $page->image = $old_imege;
          }
   
          
          $result =   $page->save();           

 //$img = '<img class="img-fluid" src="'.asset($page->image).'" alt="">';

      //  return back();
     
       // dd($request);
          return $this->index();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $page = $this->data->find($id);

        $exists = Storage::disk('frontend')->exists($page->image);   
        
        if($exists) {
            $dfl =   Storage::disk('frontend')->delete($page->image);
        } 

        $result = $page->delete();

        if ($result) {
           return $this->index();
        }


    }
}
