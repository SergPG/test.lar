<?php

namespace CleanBlog\Http\Controllers\Backend;

use Illuminate\Http\Request;
use CleanBlog\Http\Controllers\Backend\BackendController;

class DashboardController extends BackendController
{
    public function index()
    {
        //
         $this->heading = 'Information Panel';
   		 $this->subheading = 'Information the Site';

   		 $this->vars = array_add($this->vars,'heading',$this->heading);  
   		 $this->vars = array_add($this->vars,'subheading',$this->subheading);  

    	 return view('backend.dashboard.index')->with($this->vars );
    }

}
