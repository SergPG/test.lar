@extends('backend.layouts.backend')

@section('content')

  <!-- Page Header -->
	@section('page_header')
	  @include('backend.layouts.design.page_header') 
	@show 
  <!-- END Page Header -->
   
  <!-- Main Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">


<div class="card mb-3">

  <div class="card-header">
    <i class="fa fa-table"></i>
   @if (session('status'))
      {{session('status')}} Page
    @endif  
  </div> <!-- END .card-header -->

  <div class="card-body">
   <form method="POST" action="{{ route('pages.store') }}" id="form-create-page" enctype="multipart/form-data">

     @csrf

    <div class="form-group row">
      <label for="name" class="col-md-4 col-form-label text-md-right">
        {{ __('Name for Menu') }}
      </label>

      <div class="col-md-6">
        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

            @if ($errors->has('name'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('name') }}</strong>
              </span>
            @endif
      </div>
    </div> <!-- / .form-group row -->

    <div class="form-group row">
      <label for="heading" class="col-md-4 col-form-label text-md-right">
        {{ __('Heading') }}
      </label>

      <div class="col-md-6">
        <input id="heading" type="text" class="form-control{{ $errors->has('heading') ? ' is-invalid' : '' }}" name="heading" value="{{ old('heading') }}" required>

            @if ($errors->has('heading'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('heading') }}</strong>
              </span>
            @endif
      </div>
    </div> <!-- / .form-group row -->

    <div class="form-group row">
      <label for="subheading" class="col-md-4 col-form-label text-md-right">
        {{ __('Subheading') }}
      </label>

      <div class="col-md-6">
        <input id="subheading" type="text" class="form-control{{ $errors->has('subheading') ? ' is-invalid' : '' }}" name="subheading" value="{{ old('subheading') }}">

            @if ($errors->has('subheading'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('subheading') }}</strong>
              </span>
            @endif
      </div>
    </div> <!-- / .form-group row -->

    <div class="form-group row">
      <label for="text" class="col-md-4 col-form-label text-md-right">
        {{ __('Text in Page') }}
      </label>

      <div class="col-md-6">
        <textarea id="text" class="form-control{{ $errors->has('text') ? ' is-invalid' : '' }}" name="text" rows="3"> </textarea>


            @if ($errors->has('text'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('text') }}</strong>
              </span>
            @endif
      </div>
    </div> <!-- / .form-group row -->

    <div class="form-group row">
      <label for="image_page" class="col-md-4 col-form-label text-md-right">
        {{ __('Image top Page') }}
      </label>

      <div class="col-md-6">
        <input id="image_page" type="file" class="{{ $errors->has('image') ? ' is-invalid' : '' }}" name="image">

            @if ($errors->has('image'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('image') }}</strong>
              </span>
            @endif
      </div>
    </div> <!-- / .form-group row -->

    <div class="form-group row mb-0">
      <div class="col-md-6 offset-md-4">
          <button type="submit" class="btn btn-primary">
              {{ __('Save') }}
          </button>
      </div>
    </div> <!-- / .form-group row -->


   </form>

  </div>

      <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>


</div> <!-- END .card -->





       
      </div>
      </div>
    </div>
    
@endsection

		