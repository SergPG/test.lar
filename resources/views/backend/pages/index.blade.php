@extends('backend.layouts.backend')

@section('content')

  <!-- Page Header -->
	@section('page_header')
	  @include('backend.layouts.design.page_header') 
	@show 
  <!-- END Page Header -->
   
  <!-- Main Content -->
    <div class="container">
      <div class="row">

        


      <div class="col-lg-10 col-md-10 mx-auto">

@if(isset($pages) && is_object($pages))  

 @foreach($pages as $page)


      <div class="card mb-3" style="background-image: url('{{asset(config('site.storage_frontend').''.$page->image)}}')">
            <div class="card-body">

              <div class="site-heading text-white">
                  @if( isset($page->name) &&  !empty($page->name))
                    <h3>{{ $page->name }}</h3>
                  @endif
              </div>
                
              <div class="site-heading text-white text-center">  

                  @if( isset($page->heading) &&  !empty($page->heading))
                    <h1>{{ $page->heading }}</h1>
                  @endif

                  @if( isset($page->subheading) &&  !empty($page->subheading))
                    <span class="subheading">{{ $page->subheading }}</span>
                  @endif        
                 
                </div>
             </div> <!-- END .card-bod -->

          <div class="card-footer small text-muted">
             <div class="btn-group float-right" role="group" aria-label="Basic example">

                   <a id="view_page"class="btn btn-info mr-2" href="{{ route('pages.show',$page->id) }}" role="button"><i class="fa fa-lg fa-eye"  title="View"></i></a>

          <a id="edit_page"class="btn btn-warning mr-2" href="{{ route('pages.edit',$page->id) }}" role="button"><i class="fa fa-lg fa-pencil"  title="Edit"></i></a>

        
          <button type="button"  class="btn btn-danger" title="Delete" data-toggle="modal" data-target="#deleteModal-{{$page->id}}"><i class="fa fa-lg fa-trash"></i></button>


<!-- Modal -->
<div class="modal fade" id="deleteModal-{{$page->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

  <form method="POST" action="{{ route('pages.destroy', $page->id) }}" id="form-delete-page">
    @csrf
    {{ method_field('DELETE') }}

        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       Delete - {{ $page->name }}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-danger">Delete</button>
      </div>
    </div>
  </div>

   </form>
</div>




           

              </div>
          </div><!-- END .card-footer -->

        </div> <!-- END .card -->

      

 @endforeach

@endif        
      </div> <!-- END .col-lg-10 col-md-10 mx-auto -->

       
      </div>
      </div>
    
    
@endsection

		