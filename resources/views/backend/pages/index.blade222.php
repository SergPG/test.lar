@extends('backend.layouts.backend')

@section('content')

  <!-- Page Header -->
	@section('page_header')
	  @include('backend.layouts.design.page_header') 
	@show 
  <!-- END Page Header -->
   
  <!-- Main Content -->
    <div class="container">
      <div class="row">

        
          <hr>



      <div class="col-lg-10 col-md-10 mx-auto">

@if(isset($pages) && is_object($pages))  

 @foreach($pages as $page)


      <div class="card mb-3" style="background-image: url('{{asset($page->image)}}')">
            <div class="card-body">
              <div class="site-heading text-white text-center">

                  @if( isset($page->heading) &&  !empty($page->heading))
                    <h1>{{ $page->heading }}</h1>
                  @endif

                  @if( isset($page->subheading) &&  !empty($page->subheading))
                    <span class="subheading">{{ $page->subheading }}</span>
                  @endif        
                 
                </div>
             </div> <!-- END .card-bod -->

          <div class="card-footer small text-muted">
             <div class="btn-group float-right" role="group" aria-label="Basic example">

                   <a id="view_page"class="btn btn-info mr-3" href="{{ route('pages.show',$page->id) }}" role="button"><i class="fa fa-lg fa-eye"  title="View"></i></a>

          <a id="edit_page"class="btn btn-warning mr-3" href="{{ route('pages.edit',$page->id) }}" role="button"><i class="fa fa-lg fa-pencil"  title="Edit"></i></a>

                   <button type="button"  class="btn btn-danger" title="Delete"><i class="fa fa-lg fa-trash"></i></button>
              </div>
          </div><!-- END .card-footer -->

        </div> <!-- END .card -->

      

 @endforeach

@endif        
      </div> <!-- END .col-lg-10 col-md-10 mx-auto -->


         <div class="card mb-3">

          <div class="card-header">
            <i class="fa fa-table"></i> Data Table Pages

            <div class="btn-group float-right" role="group" aria-label="Basic example">
                 <a id="new_theme"class="btn btn-outline-success" href="{{ route('pages.create') }}" role="button"><i class="fa fa-lg fa-plus"  title="Add Page"></i></a>
              
            </div>

          </div> <!-- END .card-header -->

          <div class="card-body">
 @if(isset($pages) && is_object($pages))    

          <div class="table-responsive">
            <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Heading</th>
                  <th>Subheading</th>
                  
                  <th>Actions</th>
                </tr>
              </thead>

              <tfoot>
                <tr>
                  <th>Name</th>
                  <th>Heading</th>
                  <th>Subheading</th>
                  
                  <th>Actions</th>
                </tr>
              </tfoot>
              <tbody>


 @foreach($pages as $page)
                <tr>
                  <td>{{ $page->name }}</td>
                  <td>{{ $page->heading }}</td>
                  <td>{{ $page->subheading }}</td>                  
                  
          <td>
            <div class="btn-group" role="group" aria-label="Basic example">
              
          <a id="view_page"class="btn btn-outline-info" href="{{ route('pages.show',$page->id) }}" role="button"><i class="fa fa-lg fa-eye"  title="View"></i></a>

          <a id="edit_page"class="btn btn-outline-warning" href="{{ route('pages.edit',$page->id) }}" role="button"><i class="fa fa-lg fa-pencil"  title="Edit"></i></a>

          <button type="button"  class="btn btn-outline-danger" title="Delete"><i class="fa fa-lg fa-trash"></i></button>

            </div>
            
          </td>
                </tr>
               
 @endforeach
                
              </tbody>
            </table>
          </div>
@endif

        </div>

      <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>


         </div> <!-- END .card -->
       
      </div>
      </div>
    </div>
    
@endsection

		