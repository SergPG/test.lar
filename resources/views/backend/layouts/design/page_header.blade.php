<!-- Page Header -->
    <header class="masthead" style="background-image: url('{{asset('storage/backend/images/pages/about-bg.jpg')}}')">
      
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <div class="site-heading">

              @if( isset($heading) &&  !empty($heading))
                <h1>{{ $heading }}</h1>
              @endif

              @if( isset($subheading) &&  !empty($subheading))
                <span class="subheading">{{ $subheading }}</span>
              @endif      
             
              @if( isset($show_new_page))
                  <a id="new_theme"class="btn btn-success mt-3" href="{{ route('pages.create') }}" role="button" title="Add New Page"> {{ __('Add New Page') }}</a>
              @endif

 </div>
          </div>

          
        </div>
      </div>
    </header>

    