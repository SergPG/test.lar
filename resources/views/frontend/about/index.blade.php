@extends(config('set_space.theme').'.layouts.site')

@section('content')

  <!-- Page Header -->
	@section('page_header')
	  @include(config('set_space.theme').'.layouts.design.page_header') 
	@show 
  <!-- END Page Header -->
  
  <!-- About Us Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
		
          @if(!empty($currentPage->page_text))
			{!! $currentPage->page_text !!}
		  @endif
		  
        </div>
      </div>
    </div>
   <!-- END About Us  Content -->
  
  

    
	
   
@endsection

		