
  <!-- Contact Form  -->
    {!! Form::open(['route' => 'send_contact','method' => 'post','name' => 'sentMessage','id' => 'contactForm','novalidate']) !!}
       
	<div class="control-group">
      <div class="form-group floating-label-form-group controls">
	  {!! Form::label('name', 'Name') !!}
	  
	  {!! Form::text('name','',['id' => 'name','class' => 'form-control', 'placeholder' => 'Name','required', 'data-validation-required-message' => 'Please enter your name.']) !!}
	
        <p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="control-group">
      <div class="form-group floating-label-form-group controls">
		{!! Form::label('email', 'Email Address') !!}
	  
	  {!! Form::email('email','',['id' => 'email','class' => 'form-control', 'placeholder' => 'Email Address','required', 'data-validation-required-message' => 'Please enter your email address.']) !!}
        <p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="control-group">
      <div class="form-group col-xs-12 floating-label-form-group controls">
        {!! Form::label('phone', 'Phone Number') !!}
	  
	    {!! Form::tel('phone','',['id' => 'phone','class' => 'form-control', 'placeholder' => 'Phone Number','required', 'data-validation-required-message' => 'Please enter your phone number.']) !!}        
         <p class="help-block text-danger"></p>
      </div>
    </div>
    <div class="control-group">
     <div class="form-group floating-label-form-group controls">
        {!! Form::label('message', 'Message') !!}
	  
	    {!! Form::textarea('phone','',['rows' => '5','id' => 'message','class' => 'form-control', 'placeholder' => 'Message','required', 'data-validation-required-message' => 'Please enter a message.']) !!}         	
				
        <p class="help-block text-danger"></p>
     </div>
    </div>
    <br>
    <div id="success"></div>
    <div class="form-group">
	 {!! Form::submit('Send',['id' => 'sendMessageButton', 'class' => 'btn btn-primary'] ) !!}
    </div>
	
	{!! Form::close() !!}
  
  <!-- END Contact Form  -->
  
  