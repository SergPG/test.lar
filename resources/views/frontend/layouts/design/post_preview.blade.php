 <div class="post-preview">
    <a href="{{ route('show_post',$post->id)}}">
      <h2 class="post-title">
       {{ $post->title }}
      </h2>
	  @if(isset($post->subtitle ))
		<h3 class="post-subtitle">
            {{ $post->subtitle }}
		</h3>
	  @endif
    </a>
	
    <p class="post-meta">Posted by
        <a href="{{ route('home') }}">{{ config('set_space.name') }}</a>
     on  {{$post->created_at->format(config('set_space.date_format')) }}</p>
  </div>
  <hr>