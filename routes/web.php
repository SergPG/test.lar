<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend.home.index');
});



// Admin
Route::group(['prefix'=>'admin','middleware'=>'auth'],function(){
	
	//dashboard
	Route::get('/', 'Backend\DashboardController@index')->name('dashboard');

	//posts
	Route::resource('posts','Backend\PostController');

  //pages
	Route::resource('pages','Backend\PageController');
 
 //settings
	Route::resource('settings','Backend\SettingController');

});	




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
